package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        validateInputNumbers(inputNumbers);

        int rowNumbers = getRowNumber(inputNumbers);
        int columnNumbers = rowNumbers * 2 - 1;
        int[][] pyramid = new int [rowNumbers][columnNumbers];
        Collections.sort(inputNumbers);
        generatePyramid(inputNumbers, pyramid);
        return pyramid;
    }

    private void generatePyramid(List<Integer> inputNumbers, int[][] pyramid) {
        Queue<Integer> queue = new LinkedList<>(inputNumbers);
        int firstInRow = (pyramid[0].length) / 2;
        for(int i = 0; i < pyramid.length; i++)
        {
            int start = firstInRow;
            for(int j = 0; j <= i; j++)
            {
                pyramid[i][start] = queue.remove();
                start += 2;
            }
            firstInRow --;
        }
    }

    private int getRowNumber (List<Integer> inputNumbers)
    {
        int	listSize = inputNumbers.size();
        double result = (Math.sqrt(1+ 8 * listSize) - 1)/2;
        if(result == Math.ceil(result)) {
            return (int) result;
        } else {
            throw new CannotBuildPyramidException();
        }
    }

    private void validateInputNumbers(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.isEmpty() || inputNumbers.contains(null))
            throw new CannotBuildPyramidException();
    }
}
