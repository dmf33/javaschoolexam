package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

import static java.lang.Character.isDigit;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        try {
            String RPNExpression = getRPNExpression(statement);
            return getResult(RPNExpression);
        } catch (Exception e) {
            return null;
        }
    }

    private String getRPNExpression(String inputExpression) {

        Stack<Character> operations = new Stack<>();

        return getRPN(inputExpression, operations);
    }

    private String getRPN(String inputExpression, Stack<Character> operations) {
        StringBuilder expression = new StringBuilder();
        for (int i = 0; i < inputExpression.length(); i++) {

            if (isDigit(inputExpression.charAt(i))) {
                while (!isOperand(inputExpression.charAt(i))) {
                    expression.append(inputExpression.charAt(i));
                    i++;

                    if (i == inputExpression.length()) {
                        break;
                    }
                }

                i--;
                expression.append(" ");
            }

            if (isOperand(inputExpression.charAt(i))) {

                if (inputExpression.charAt(i) == '(') {
                    operations.push(inputExpression.charAt(i));
                } else if (inputExpression.charAt(i) == ')') {
                    Character character = operations.pop();

                    while (character != '(') {
                        expression.append(character.toString()).append(" ");
                        character = operations.pop();
                    }
                } else {
                    if (!operations.empty()) {
                        if ((getPriority(inputExpression.charAt(i)) <= getPriority(operations.peek()))) {

                            expression.append(operations.pop().toString()).append(" ");
                        }
                    }
                    operations.push(inputExpression.charAt(i));
                }
            }
        }

        while (!operations.empty()) {
            expression.append(operations.pop().toString()).append(" ");
        }
        return expression.toString();
    }

    private byte getPriority(Character s) {
        switch (s) {
            case '(':
                return 0;
            case ')':
                return 1;
            case '+':
                return 2;
            case '-':
                return 3;
            case '*':
                return 4;
            case '/':
                return 5;
            default:
                return 6;
        }
    }

    private String getResult(String RPNExpression) {
        double result = 0;
        Stack<Double> numbers = new Stack<>();

        for (int i = 0; i < RPNExpression.length(); i++) {

            if (isDigit(RPNExpression.charAt(i))) {
                StringBuilder number = new StringBuilder();

                while ((!isOperand(RPNExpression.charAt(i))) && (!isDelimeter(RPNExpression.charAt(i)))) {
                    number.append(RPNExpression.charAt(i));
                    i++;

                    if (i == RPNExpression.length()) {
                        break;
                    }
                }

                numbers.push(Double.parseDouble(number.toString()));
                i--;
            } else if (isOperand(RPNExpression.charAt(i))) {
                double firstNumber = numbers.pop();
                double secondNumber = numbers.pop();

                switch (RPNExpression.charAt(i)) {
                    case '+':
                        result = firstNumber + secondNumber;
                        break;
                    case '-':
                        result = secondNumber - firstNumber;
                        break;
                    case '/':
                        if(firstNumber == 0){
                            return null;
                        }
                        result = secondNumber / firstNumber;
                        break;
                    case '*':
                        result = firstNumber * secondNumber;
                        break;
                }
                numbers.push(result);
            }
        }

        String a = numbers.pop().toString();
        int b = a.indexOf(".");

        if(a.length() - b - 1 > 1 ){
            return a;
        } else {
            int resultDouble = (int)Double.parseDouble(a);
            return Integer.toString(resultDouble);
        }

    }

    private boolean isDelimeter(Character s) {
        return s.equals(' ');
    }

    private boolean isOperand(Character s) {
        switch (s) {
            case '(':
            case '/':
            case ')':
            case '+':
            case '-':
            case '*':
                return true;
            default:
                return false;
        }
    }
}
